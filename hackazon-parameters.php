<?php
return array (
  'host' => 'http://hackazon:80/',
  'display_errors' => true,
  'use_perl_upload' => false,
  'use_external_dir' => false,
  'user_pictures_external_dir' => '/lib/init/rw',
  'user_pictures_path' => '/web/user_pictures/',
  'common_path' => '/var/www/hackazon/assets/views/common/',
  'annotation_length' => 900,
  'rest_in_profile' => false,
  'profile_rest_data_type' => 'xml',
  'test_user' => 
  array (
    'username' => 'test_user',
    'password' => '123456',
  ),
  'installer_password' => 'youradminpass',
);
