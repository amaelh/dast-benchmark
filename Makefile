.PHONY: default run

BUILD_DIR := $(abspath $(dir $(lastword $(MAKEFILE_LIST))))

default: run

run:
	mkdir -p $(BUILD_DIR)/output/juice-shop
	mkdir -p $(BUILD_DIR)/output/hackazon
	mkdir -p $(BUILD_DIR)/output/webgoat
	find $(BUILD_DIR)/output/ -user $(shell id -u) | xargs -I TOTO chmod a+w TOTO
	docker-compose down && docker-compose up --remove-orphans
