#!/bin/sh

echo "[INFO] Starting custom webgoat init script with attacker name : ${ATTACKER_NAME}"

# Automation user
curl -X POST -H "Content-Type: application/json" \
  -d "username=${ATTACKER_NAME}" \
  -d "password=${ATTACKER_NAME}" \
  -d "matchingPassword=${ATTACKER_NAME}" \
  -d "agree=agree" \
  http://localhost:8080/WebGoat/register.mvc

echo "[INFO] End of custom WebGoat user-init script"
