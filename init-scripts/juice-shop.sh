#!/bin/sh

echo "[INFO] Starting Juice-shop user-init script with attacker name : ${ATTACKER_NAME}"

# Automation user
curl -X POST -H "Content-Type: application/json" -d \
  "{\"email\":\"${ATTACKER_NAME}@${ATTACKER_NAME}.com\",\"password\":\"${ATTACKER_NAME}\",\"passwordRepeat\":\"${ATTACKER_NAME}\",\"securityQuestion\":{\"id\":2,\"question\":\"Name of your favorite pet?\",\"createdAt\":\"2021-12-27T13:01:19.659Z\",\"updatedAt\":\"2021-12-27T13:01:19.659Z\"},\"securityAnswer\":\"${ATTACKER_NAME}\"}" \
  http://juice-shop:3000/api/Users/

echo "[INFO] End of Juice-shop user-init script"
