#!/bin/sh

# Optionnal environment variable
if [ -z ${STARTUP_DELAY} ]
then
  echo "[INFO] Environment variable STARTUP_DELAY is not set, starting scanner immediately"
else
  echo "[INFO] Sleeping ${STARTUP_DELAY} seconds..."
  sleep ${STARTUP_DELAY}
fi

# Optionnal : make a target page snapshot to check if it runs correctly
if [ ! -z ${MAKE_SNAPSHOT} ]
then
  vl_url=$(echo $@ | cut -d' ' -f 1)
  vl_title="$(echo ${vl_url} | cut -d'/' -f 3 | sed "s#:#_#")_$(date +%m%d%Y_%H%M%S)_root-page-sample.html"
  echo "[INFO] Making a snapshot of ${vl_url} under the filename ${vl_title}"
  wget ${vl_url} -O /root/.wapiti/generated_report/${vl_title}
fi

# Run victim-specific init scripts
echo "[INFO] Checking for victim-specific init scripts in /init-scripts..."
for vl_script in $(find /init-scripts -type f)
do
  test -x ${vl_script} && echo "[INFO] Running ${vl_script}..." && ${vl_script}
  test ! -x ${vl_script} && echo "[WARNING] File ${vl_script} is not executable, skipping"
done

# Starting analysis
echo "[INFO] Executing command"
$@

